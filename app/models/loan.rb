class Loan < ActiveRecord::Base
	include ActionView::Helpers::NumberHelper
	
	has_many :payments, dependent: :destroy
	validates_presence_of :name, :last_name, :phone, :email, :personal_id, :credit_sum
	before_save :calculate_graph

	def calculate_graph
		credit_start_date = loan_start_date

		credit_intress = 0.1

		# monhtly payment
		year_percent = credit_intress / 12 
		rank = ( 1 + year_percent )**credit_period - 1
		monthly_payment = credit_sum * ( year_percent + (year_percent / rank))

		intress_payment = monthly_payment * credit_period - credit_sum
		# percent payment
		percent = credit_sum * year_percent
		# balance 
		balance = credit_sum
		# main part of payment
		principal_repayment	= monthly_payment - percent
		
		
		credit_period.times do
			
			credit_start_date = credit_start_date + 1.month

			payments.build({
				payment_date: credit_start_date.strftime('%d.%m.%Y'),
				monthly_paymnet_total: number_to_currency(monthly_payment, precision: 2, :unit => "", :delimiter => ""),
				payment_balance: number_to_currency(balance, precision: 2, :unit => "", :delimiter => ""),
				intress_payment: number_to_currency(percent, precision: 2, :unit => "", :delimiter => ""),
				princial_repayment: number_to_currency(principal_repayment, precision: 2, :unit => "", :delimiter => "")
			})

			# Recounting with new balance
			balance -= principal_repayment
			percent = balance * year_percent
			principal_repayment = monthly_payment - percent

		end

		return payments	
	end

	def loan_start_date
		DateTime.now
	end

end
