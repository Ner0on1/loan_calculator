# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('.btn-primary').on 'click', ->
  	credit_sum = $('#loan_credit_sum').val()
  	credit_period = $('#loan_credit_period').val()
  	if loan_sum(credit_sum)
	  	$.ajax({
	  		type: "GET"
				url: "/show_graph",
				data: { loan: {credit_sum: credit_sum, credit_period: credit_period} },
				dataType: "json"
				success: (data) ->
					$("#payments_table tbody").empty()
					$("#payments_table tfoot").empty()

					if data?
						for x in data.payments
							$("#payments_table tbody").append $("<tr><td>#{x.payment_date}</td><td>#{x.payment_balance}</td><td>#{x.princial_repayment}</td><td>#{x.intress_payment}</td><td>#{x.monthly_paymnet_total}</td></tr>")
					total_months = x.monthly_paymnet_total * credit_period
					$("#payments_table tfoot").append $("<tr><td><b>Total:</b></td><td></td><td></td><td></td><td><b>#{total_months}</b></td></tr>")

				error: (data) ->
					return false
  	});


	loan_sum = (loan_sum) ->
	  if loan_sum > 10000
	  	alert('Loan amount cannot be more then 10 000 ');
	  else
	  	return true