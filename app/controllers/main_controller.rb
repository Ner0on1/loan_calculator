class MainController < ApplicationController
	def index
		@new_loan = Loan.new
		@loans = Loan.all
	end

	def create
		@new_loan = Loan.new loan_params

		@new_loan.credit_start_date = DateTime.now

		if @new_loan.save
			redirect_to :back
		else
			render :index		
		end

	end
	
	def show
		@loan = Loan.find(params[:id])
		@payments = @loan.payments
	end

	def destroy
		@loan = Loan.find(params[:id])
		@loan.destroy

		redirect_to :back
	end
	
	def show_graph
		@new_loan = Loan.new loan_params
		@new_loan.calculate_graph
		render :json => @new_loan.to_json({:include => :payments})
	end

	def loan_params
		params.require(:loan).permit(:name,:last_name,:email,:phone,:personal_id,:credit_sum,:credit_period,:credit_start_date)
	end

end
# amount