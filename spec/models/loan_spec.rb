require "rails_helper"

RSpec.describe Loan, :type => :model do

	it "calculate payments with 12 months period" do 
		loan = Loan.new(credit_sum: 5000, credit_period: 12)
		graph = loan.calculate_graph
		# Monthly payment
		expect(graph[0].monthly_paymnet_total).to eq(439.58)
		# Total Monthly payments
		expect(graph.map(&:monthly_paymnet_total).inject(:+)).to eq(5274.96)
	end

	it "calculate payments with 60 months period" do 
		loan = Loan.new(credit_sum: 5000, credit_period: 60)
		graph = loan.calculate_graph
		# Monthly payment
		expect(graph[0].monthly_paymnet_total).to eq(106.24)
		# Total Monthly payments
		expect(graph.map(&:monthly_paymnet_total).inject(:+).to_i).to eq(6374)
	end

end