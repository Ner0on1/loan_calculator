class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
    	t.string   "name"
	    t.string   "last_name"
	    t.integer  "string"
	    t.string   "personal_id"
	    t.string   "email"
	    t.float    "credit_sum"
	    t.integer  "credit_period"
	    t.date     "credit_start_date"
	    t.integer  "credit_intress",    default: 10
    t.timestamps null: false
    end
  end
end
